from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

from .municipio import Municipio

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    email = models.CharField('email', max_length = 100, unique=True)
    password = models.CharField('Password', max_length = 256)
    nombre = models.CharField('Nombre', max_length = 60, default="")
    idmunicipio = models.ForeignKey(Municipio, related_name='user', on_delete=models.CASCADE, default=0)
    campesino = models.BooleanField('Campesino', default=False)
    comprador = models.BooleanField('Comprador', default=True)
    administrador = models.BooleanField('Administrador', default=False)

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'email'
