from django.db import models

class Departamento(models.Model):
    iddepartamento = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length=50, unique=True, default="")
