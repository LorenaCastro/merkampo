from django.db import models

from .departamento import Departamento

class Municipio(models.Model):
    idmunicipio = models.AutoField(primary_key=True)
    iddepartamento = models.ForeignKey(Departamento, related_name='municipio', on_delete=models.CASCADE, default=0)
    nombre = models.CharField('Nombre', max_length=50, unique=True, default="")
