from rest_framework import serializers
from authApp.models.municipio import Municipio

class MunicipioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipio
        fields = ['idmunicipio', 'iddepartamento', 'nombre']


    def to_representation(self, obj):
        municipio = Municipio.objects.get(idmunicipio=obj.idmunicipio)
        return {
                    'idmunicipio': municipio.idmunicipio,
                    'iddepartamento': municipio.iddepartamento,
                    'nombre': municipio.nombre,
                }
