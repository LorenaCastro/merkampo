from rest_framework import serializers
from authApp.models.departamento import Departamento

class DepartamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamento
        fields = ['iddepartamento', 'nombre']


    def to_representation(self, obj):
        departamento = Departamento.objects.get(iddepartamento=obj.iddepartamento)
        return {
                    'iddepartamento': departamento.iddepartamento,
                    'nombre': departamento.nombre,
                }
