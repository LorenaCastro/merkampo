from rest_framework import serializers
from authApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'password', 'nombre', 'idmunicipio', 'campesino', 'comprador', 'administrador']


    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
                    'id': user.id, 
                    'email': user.email,
                    'nombre': user.nombre,
                    'idmunicipio': user.idmunicipio,
                    'campesino': user.campesino,
                    'comprador': user.comprador,
                    'administrador': user.administrador,
                }
