from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from authApp.serializers import MunicipioSerializer
from authApp.models.municipio import Municipio

class MunicipioListApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the municipios records
        '''
        municipios = Municipio.objects.all()
        serializer = MunicipioSerializer(municipios, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create
    def post(self, request, *args, **kwargs):
        '''
        Create the municipio with given municipio data
        '''
        data = {
            'idmunicipio': request.data.get('idmunicipio'), 
            'iddepartamento': request.data.get('iddepartamento'), 
            'nombre': request.data.get('nombre'), 
        }
    
        serializer = MunicipioSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)