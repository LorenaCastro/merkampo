from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from authApp.serializers import DepartamentoSerializer
from authApp.models.departamento import Departamento

class DepartamentoDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, dpto_id):
        '''
        Helper method to get the object with given dpto_id, and user_id
        '''
        try:
            return Departamento.objects.get(iddepartamento = dpto_id)
        except Departamento.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, dpto_id, *args, **kwargs):
        '''
        Retrieves the departamento with given dpto_id
        '''
        departamento_instance = self.get_object(dpto_id)
        if not departamento_instance:
            return Response(
                {"res": "Object with departamento id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = DepartamentoSerializer(departamento_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, dpto_id, *args, **kwargs):
        '''
        Updates the departamento item with given dpto_id if exists
        '''
        departamento_instance = self.get_object(dpto_id)
        if not departamento_instance:
            return Response(
                {"res": "Objeto con departamento id does not exist"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'nombre': request.data.get('nombre'),
        }
        serializer = DepartamentoSerializer(instance = departamento_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, dpto_id, *args, **kwargs):
        '''
        Deletes the departamento item with given todo_id if exists
        '''
        departamento_instance = self.get_object(dpto_id)
        if not departamento_instance:
            return Response(
                {"res": "Object with departamento id does not exist"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        departamento_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )