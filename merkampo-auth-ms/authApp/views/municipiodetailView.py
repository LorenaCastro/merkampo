from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from authApp.serializers import MunicipioSerializer
from authApp.models.municipio import Municipio

class MunicipioDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, mpio_id):
        '''
        Helper method to get the object with given mpio_id, and user_id
        '''
        try:
            return Municipio.objects.get(idmunicipio = mpio_id)
        except Municipio.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, mpio_id, *args, **kwargs):
        '''
        Retrieves the municipio with given mpio_id
        '''
        municipio_instance = self.get_object(mpio_id)
        if not municipio_instance:
            return Response(
                {"res": "Object with municipio id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = MunicipioSerializer(municipio_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, mpio_id, *args, **kwargs):
        '''
        Updates the municipio item with given mpio_id if exists
        '''
        municipio_instance = self.get_object(mpio_id)
        if not municipio_instance:
            return Response(
                {"res": "Objeto con municipio id does not exist"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'iddepartamento': request.data.get('iddepartamento'),
            'nombre': request.data.get('nombre'),
        }
        serializer = MunicipioSerializer(instance = municipio_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, mpio_id, *args, **kwargs):
        '''
        Deletes the municipio item with given mpio_id if exists
        '''
        municipio_instance = self.get_object(mpio_id)
        if not municipio_instance:
            return Response(
                {"res": "Object with municipio id does not exist"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        municipio_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )