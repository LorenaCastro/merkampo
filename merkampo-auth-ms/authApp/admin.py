from django.contrib import admin
from .models.user import User
from .models.municipio import Municipio
from .models.departamento import Departamento

admin.site.register(User)
admin.site.register(Municipio)
admin.site.register(Departamento)
